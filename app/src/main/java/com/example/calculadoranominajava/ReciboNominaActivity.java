package com.example.calculadoranominajava;

import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

public class ReciboNominaActivity extends AppCompatActivity {

    // Declaración de variables de componentes de la interfaz de usuario
    private EditText txtNumRecibo;
    private EditText txtNombre;
    private EditText txtHorasNormales;
    private EditText txtHorasExtra;
    private RadioGroup rdbPuesto;
    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private TextView lblSubtotalTotal;
    private TextView lblImpuestoTotal;
    private TextView lblTotalTotal;
    private TextView lblUsuario;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo_nomina);

        // Inicialización de componentes de la interfaz de usuario
        iniciarComponentes();

        // Obtención del usuario de la actividad anterior
        Bundle datos = getIntent().getExtras();
        String usuario = datos.getString("usuario");
        lblUsuario.setText(usuario);

        // Manejo de eventos de los botones
        btnCalcular.setOnClickListener(v -> {
            if (validarCampos()) {
                calcularNomina();
            } else {
                mostrarToast("Por favor, completa todos los campos");
            }
        });

        btnLimpiar.setOnClickListener(v -> limpiarCampos());
        btnRegresar.setOnClickListener(v -> regresar());
    }

    private void iniciarComponentes() {
        // Asignación de componentes de la interfaz a las variables
        txtNumRecibo = findViewById(R.id.txtNumRecibo);
        txtNombre = findViewById(R.id.txtNombre);
        txtHorasNormales = findViewById(R.id.txtHorasNormales);
        txtHorasExtra = findViewById(R.id.txtHorasExtra);
        rdbPuesto = findViewById(R.id.rdbPuesto);
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        lblSubtotalTotal = findViewById(R.id.lblSubtotalTotal);
        lblImpuestoTotal = findViewById(R.id.lblImpuestoTotal);
        lblTotalTotal = findViewById(R.id.lblTotalTotal);
        lblUsuario = findViewById(R.id.lblUsuario);
    }

    private boolean validarCampos() {
        // Validación de campos: se verifica si todos los campos están completos
        String numRecibo = txtNumRecibo.getText().toString();
        String nombre = txtNombre.getText().toString();
        String horasNormales = txtHorasNormales.getText().toString();
        String horasExtra = txtHorasExtra.getText().toString();
        int selectedRadioButtonId = rdbPuesto.getCheckedRadioButtonId();
        return !numRecibo.isEmpty() && !nombre.isEmpty() && !horasNormales.isEmpty() && !horasExtra.isEmpty() && selectedRadioButtonId != -1;
    }

    private void calcularNomina() {
        // Cálculo de la nómina en base a los datos ingresados
        int numRecibo = Integer.parseInt(txtNumRecibo.getText().toString());
        String nombre = txtNombre.getText().toString();
        float horasNormales = Float.parseFloat(txtHorasNormales.getText().toString());
        float horasExtra = Float.parseFloat(txtHorasExtra.getText().toString());
        int selectedRadioButtonId = rdbPuesto.getCheckedRadioButtonId();
        int puesto = 0;

        if (selectedRadioButtonId != -1) {
            RadioButton selectedRadioButton = findViewById(selectedRadioButtonId);
            String puestoText = selectedRadioButton.getText().toString();

            // Asignación de un valor numérico al puesto según la opción seleccionada
            switch (puestoText.toLowerCase()) {
                case "auxiliar":
                    puesto = 1;
                    break;
                case "albañil":
                    puesto = 2;
                    break;
                case "ing obra":
                    puesto = 3;
                    break;
            }
        }

        // Creación de un objeto ReciboNomina y cálculo de los valores
        ReciboNomina reciboNomina = new ReciboNomina(numRecibo, nombre, horasNormales, horasExtra, puesto, 16f);
        float subtotal = reciboNomina.calcularSubtotal();
        float impuesto = reciboNomina.calcularImpuesto();
        float total = reciboNomina.calcularTotal();

        // Actualización de las etiquetas de total y subtotal en la interfaz
        lblSubtotalTotal.setText(String.valueOf(subtotal));
        lblImpuestoTotal.setText(String.valueOf(impuesto));
        lblTotalTotal.setText(String.valueOf(total));
    }

    private void limpiarCampos() {
        // Limpieza de los campos de la interfaz
        txtNumRecibo.setText("");
        txtNombre.setText("");
        txtHorasNormales.setText("");
        txtHorasExtra.setText("");
        rdbPuesto.clearCheck();
        lblSubtotalTotal.setText("");
        lblImpuestoTotal.setText("");
        lblTotalTotal.setText("");
    }

    private void regresar() {
        // Diálogo de confirmación al regresar al menú principal
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Calculadora Nómina");
        confirmar.setMessage("¿Regresar al Menú Principal?");
        confirmar.setPositiveButton("Confirmar", (dialog, which) -> finish());
        confirmar.setNegativeButton("Cancelar", (dialog, which) -> dialog.dismiss());
        confirmar.show();
    }

    private void mostrarToast(String mensaje) {
        // Mostrar un mensaje Toast en la pantalla
        Toast.makeText(getApplicationContext(), mensaje, Toast.LENGTH_SHORT).show();
    }
}
