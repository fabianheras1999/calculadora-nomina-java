package com.example.calculadoranominajava;

import java.text.DecimalFormat;

public class ReciboNomina {
    private int numRecibo;
    private String nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;
    private float impuestoPorc;

    public ReciboNomina(int numRecibo, String nombre, float horasTrabNormal, float horasTrabExtras, int puesto, float impuestoPorc) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }

    public float reciboNominal() {
        float pagoBase = 200f;
        switch (puesto) {
            case 1:
                return pagoBase * 1.2f;
            case 2:
                return pagoBase * 1.5f;
            case 3:
                return pagoBase * 2.0f;
            default:
                return pagoBase;
        }
    }

    public float calcularSubtotal() {
        float pagoBase = reciboNominal();
        float pagoHoraExtra = pagoBase * 2;
        float subtotal = pagoBase * horasTrabNormal + pagoHoraExtra * horasTrabExtras;
        return formatFloat(subtotal);
    }

    public float calcularImpuesto() {
        float subtotal = calcularSubtotal();
        float impues = subtotal * (impuestoPorc / 100);
        return formatFloat(impues);
    }

    public float calcularTotal() {
        float subtotal = calcularSubtotal();
        float impuesto = calcularImpuesto();
        float total = subtotal - impuesto;
        return formatFloat(total);
    }

    private float formatFloat(float value) {
        DecimalFormat decimalFormat = new DecimalFormat("#.00");
        return Float.parseFloat(decimalFormat.format(value));
    }

    // Getters y Setters

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }
}
